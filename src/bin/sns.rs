use goose::prelude::*;

async fn search(user: &mut GooseUser) -> TransactionResult {
    let _goose_metrics = user.get("de/search.html?q=wasser").await?;

    Ok(())
}

async fn concept_html(user: &mut GooseUser) -> TransactionResult {
    let _goose_metrics = user.get("de/concepts/_00028954.html").await?;

    Ok(())
}

async fn concept_rdf(user: &mut GooseUser) -> TransactionResult {
    let _goose_metrics = user.get("de/concepts/_00028954.rdf").await?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), GooseError> {
    GooseAttack::initialize()?
        .register_scenario(
            scenario!("all")
                .register_transaction(transaction!(search))
                .register_transaction(transaction!(concept_html))
                .register_transaction(transaction!(concept_rdf)),
        )
        .execute()
        .await?;

    Ok(())
}
