use goose::prelude::*;
use reqwest::Client;

async fn http2_prior_knowledge(user: &mut GooseUser) -> TransactionResult {
    let builder = Client::builder().http2_prior_knowledge();

    user.set_client_builder(builder).await?;

    Ok(())
}

async fn login(user: &mut GooseUser) -> TransactionResult {
    let _goose_metrics = user.get("").await?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), GooseError> {
    GooseAttack::initialize()?
        .register_scenario(
            scenario!("all")
                .register_transaction(transaction!(http2_prior_knowledge).set_on_start())
                .register_transaction(transaction!(login)),
        )
        .execute()
        .await?;

    Ok(())
}
