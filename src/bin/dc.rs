use goose::prelude::*;
use serde_json::json;

async fn explorer(user: &mut GooseUser) -> TransactionResult {
    user.base_url.set_port(Some(7001)).unwrap();

    let _goose_metrics = user.get("").await?;

    Ok(())
}

async fn search(user: &mut GooseUser) -> TransactionResult {
    user.base_url.set_port(Some(3004)).unwrap();

    let request_body = json!({
        "lang":"en",
        "search":"",
        "sort":"score desc, sname asc, indexationDate desc",
        "facets":{
            "UBA Categories":["0|Private housholds and consumption#PRIVATE_HOUSHOLDS_CONSUMPTION#"],
            "datasourceId":["ds-dc-design","ds-dc-release"]
        },
        "rows":20,
        "start":0
    });

    let _goose_metrics = user.post_json("api/search/", &request_body).await?;

    Ok(())
}

async fn release(user: &mut GooseUser) -> TransactionResult {
    user.base_url.set_port(Some(8881)).unwrap();

    let _goose_metrics = user
        .get("rest/availableconstraint/UBA,DF_CONSUMPTION_SPENDING_USE,1.0/all")
        .await?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), GooseError> {
    GooseAttack::initialize()?
        .register_scenario(
            scenario!("all")
                .register_transaction(transaction!(explorer))
                .register_transaction(transaction!(search))
                .register_transaction(transaction!(release)),
        )
        .execute()
        .await?;

    Ok(())
}
