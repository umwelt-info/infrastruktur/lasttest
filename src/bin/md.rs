use goose::prelude::*;

async fn search_all(user: &mut GooseUser) -> TransactionResult {
    let request_builder = user
        .get_request_builder(&GooseMethod::Get, "search")?
        .header("accept", "application/json");

    let goose_request = GooseRequest::builder()
        .set_request_builder(request_builder)
        .expect_status_code(200)
        .build();

    let _goose_metrics = user.request(goose_request).await?;

    Ok(())
}

async fn search_one(user: &mut GooseUser) -> TransactionResult {
    let request_builder = user
        .get_request_builder(&GooseMethod::Get, "search?query=Buche")?
        .header("accept", "application/json");

    let goose_request = GooseRequest::builder()
        .set_request_builder(request_builder)
        .expect_status_code(200)
        .build();

    let _goose_metrics = user.request(goose_request).await?;

    Ok(())
}

async fn dataset_one(user: &mut GooseUser) -> TransactionResult {
    let request_builder = user
        .get_request_builder(
            &GooseMethod::Get,
            "dataset/geodatenkatalog/8B233EF4-F68D-4C84-A69B-06D35741577A",
        )?
        .header("accept", "application/json");

    let goose_request = GooseRequest::builder()
        .set_request_builder(request_builder)
        .expect_status_code(200)
        .build();

    let _goose_metrics = user.request(goose_request).await?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), GooseError> {
    GooseAttack::initialize()?
        .register_scenario(
            scenario!("all")
                .register_transaction(transaction!(search_all))
                .register_transaction(transaction!(search_one))
                .register_transaction(transaction!(dataset_one)),
        )
        .execute()
        .await?;

    Ok(())
}
